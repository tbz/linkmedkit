# LinkMedkit

A set of bash scripts for repository badges. These scripts read input from file in json format conforming a [schema](https://gitlab.mpcdf.mpg.de/tbz/linkmedkit/-/blob/main/tests/badge.schema.json) compatible with [shields.io json format](https://github.com/badges/shields/blob/2c0737592b29e78f5c543a0e5ed908149dd18436/doc/json-format.md).

- `badger.sh`: generates a SVG badge.
- `gitlab_badge_sticker.sh`: updates GitLab repository badges.

## Quick start

### install dependecies

1. Install `jq`
2. Install `python3` and python3 package manager e.g. `pip`
3. Install python dependecies:

```shell
pip install -r requirements.txt
```

### prepare input

Example badge file `badge.test.json`:

```json
{
    "schemaVersion": 1,
    "label": "dead%20internal%20links",
    "message": 0,
    "color": "green"
}
```

### generate svg badge

```shell
badger.sh --badge-info="badge.test.json"
```

It will generate `test.svg` file as: ![test.svg](https://gitlab.mpcdf.mpg.de/tbz/linkmedkit/-/raw/main/examples/test.svg)

### update GitLab repository badge

```shell
gitlab_badge_sticker.sh
```

## Usage & Options

### Badger

- Display help. Will show all the command line options and their default values.

```shell
badger.sh -h
```

- Read `badge.dead_internal_links.json` file, create a badge file named `dead_internal_links.svg`.

```shell
badger.sh
```

- Read the number of dead links from `badge.dead_external_links.json` file.

```shell
badger.sh --badge-info="badge.dead_external_links.json"
```

- Generate badge file `public/dead_external.svg`. Create the parent directories if missing.

```shell
badger.sh --badge-file="public/dead_external.svg"
```

- Run `badger.sh` in verbose mode

```shell
badger.sh --verbose
```

### GitLab Badge Updater

- Find files named `badge.*.json` and update GitLab repository badges named (`*`) or creates them if not found. The badges are set to be rendered by [shields.io](https://shields.io/) and will be linked to the GitLab CI job which has set them.

  This script requires a read/write [API access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) called `CI_API_TOKEN` for the project with [maintainer permission](https://docs.gitlab.com/ee/user/permissions.html) to modify the badges. Please add the required API access token to your project and set it as a [masked variable](https://docs.gitlab.com/ee/ci/variables/) in CI.

```shell
gitlab_badge_sticker.sh
```

- Run `gitlab_badge_sticker.sh` in verbose mode.

```shell
gitlab_badge_sticker.sh --verbose
```

## Background

These scripts were originally developed for [linkmedic](https://gitlab.mpcdf.mpg.de/tbz/linkmedic).

## License

Copyright 2021-2023 M. Farzalipour Tabriz, Max Planck Computing and Data Facility (MPCDF)

All rights reserved.

This software may be modified and distributed under the terms of the 3-Clause BSD License. See the LICENSE file for details.
