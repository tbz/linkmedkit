#!/bin/bash

# Copyright 2021-2023 M. Farzalipour Tabriz, Max Planck Computing and Data Facility (MPCDF)
# All rights reserved.
#
# This software may be modified and distributed under the terms
# of the 3-Clause BSD License. See the LICENSE file for details.
# CLI parser is generated using Argbash v2.9.0 (BSD-3-Clause) https://github.com/matejak/argbash

set -e

version="0.3.1"
script_name="Badger: SVG Badge Generator"


die()
{
	local _ret="${2:-1}"
	test "${_PRINT_HELP:-no}" = yes && print_help >&2
	echo "$1" >&2
	exit "${_ret}"
}


# default options
_arg_badge_info_file="badge.dead_internal_links.json"
_arg_verbose="off"

print_help()
{
	printf '%s\n' "Badger: SVG badge generator for link medic"
	printf 'Usage: %s [--badge-info <arg>] [--badge-file <arg>] [-v|--version] [--verbose] [-h|--help]\n' "$0"
	printf '\t%s\n' "--badge-info: badge information file name in 'badge.[***].json' format. [***] is interpreted as the badge-name (default: 'badge.dead_internal_links.json')"
	printf '\t%s\n' "--badge-file: output badge file name (default: '[badge-name].svg')"
	printf '\t%s\n' "-v, --version: Prints version"
	printf '\t%s\n' "--verbose: verbose mode"
	printf '\t%s\n' "-h, --help: Prints help"
}


parse_commandline()
{
	while test $# -gt 0
	do
		_key="$1"
		case "$_key" in
			--badge-info)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_badge_info_file="$2"
				shift
				;;
			--badge-info=*)
				_arg_badge_info_file="${_key##--badge-info=}"
				;;
			--badge-file)
				test $# -lt 2 && die "Missing value for the optional argument '$_key'." 1
				_arg_badge_file="$2"
				shift
				;;
			--badge-file=*)
				_arg_badge_file="${_key##--badge-file=}"
				;;
			-v|--version)
				echo "$script_name" "v$version"
				exit 0
				;;
			-v*)
				echo "$script_name" "v$version"
				exit 0
				;;
			--verbose)
				_arg_verbose="on"
				;;
			-h|--help)
				print_help
				exit 0
				;;
			-h*)
				print_help
				exit 0
				;;
			*)
				_PRINT_HELP=yes die "FATAL ERROR: Got an unexpected argument '$1'" 1
				;;
		esac
		shift
	done
}

parse_commandline "$@"

if [ ! -f "${_arg_badge_info_file}" ]; then
	echo "ERROR: badge info file is missing: ${_arg_badge_info_file}"
	exit 1
else
	badge_name_json=${_arg_badge_info_file##*badge.}
	badge_name=${badge_name_json%.json}
	badge_label=$(jq -r '.label' <"$_arg_badge_info_file")
	badge_value=$(jq -r '.message' <"$_arg_badge_info_file")
	badge_color=$(jq -r '.color' <"$_arg_badge_info_file")
fi

if [ -z "$_arg_badge_file" ]; then
	_arg_badge_file="$badge_name.svg"
fi

sanitized_badge_label=${badge_label//%20/ }

if [ "$_arg_verbose" == "on" ]; then
	echo "badge info file: $_arg_badge_info_file"
	echo "badge name: $badge_name"
	echo "badge label: $sanitized_badge_label"
	echo "badge value: $badge_value"
	echo "badge color: $badge_color"
	echo "Output image file: $_arg_badge_file"
fi

mkdir -p "$(dirname "${_arg_badge_file}")"

command python3 -m pybadges --left-text="$sanitized_badge_label" --right-text="${badge_value}" --right-color="${badge_color}" > "${_arg_badge_file}"
