#! /bin/bash

# gitlab badge updater script for running in CI
# finds files named badge.*.json and 
# updates repository badges named (*) or creates them if not found
#
# requires tools:
## curl
## jq
#
# required env vars:
## CI_SERVER_HOST
## CI_PROJECT_ID
## CI_JOB_URL
## CI_API_TOKEN

set -e
set -o pipefail
shopt -s nullglob

# DEFAULT VALUES
SHIELDS_SERVER="img.shields.io"
BADGES_PATH="." # badge.*.json files are read from this path

if [[ "$1" == "--verbose" ]]; then
    verbose=1
fi

required_env_variables=("CI_SERVER_HOST" "CI_PROJECT_ID" "CI_JOB_URL")

for variable in "${required_env_variables[@]}"; do
    if [[ -z ${!variable} ]]; then
        echo "ERROR: \$${variable} is not set!"
        env_err=1
    fi
done

if [[ -n $env_err ]]; then
    echo "This script is meant to be run in GitLab CI environment. You must set the required variables manually if you want to use it elsewhere."
    exit 1;
fi

if [[ -z "${CI_API_TOKEN}" ]]; then
    echo "ERROR: \$CI_API_TOKEN is not set!"
    echo "This script requires a read/write API access token for the project with maintainer permission for modifying badges. Please add a token to your project and set it as a masked variable in CI."
    echo "See GitLab documentation for more details:"
    echo "https://docs.gitlab.com/ee/user/permissions.html"
    echo "https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html"
    echo "https://docs.gitlab.com/ee/ci/variables/"
    exit 1;
fi

TMP_FILE=$(mktemp)

badge_info_files_list=$(find "${BADGES_PATH}" -maxdepth 1 -name 'badge.*.json' -type f)
if [[ "$badge_info_files_list" == '' ]]; then
    echo "ERROR: no badge info file ('badge.*.json') was found in the path: $BADGES_PATH"
    echo "path expands as: $(realpath "$BADGES_PATH")"
    ls -lah "$BADGES_PATH"
    exit 1
fi

[[ -z $verbose ]] || echo "reading old badges from the server: ${CI_SERVER_HOST}"
curl -s -S --fail --header "PRIVATE-TOKEN: ${CI_API_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/badges" > "$TMP_FILE" || curl --verbose --fail --header "PRIVATE-TOKEN: ${CI_API_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/badges"

[[ -z $verbose ]] || printf "discovered local badge info files:%s\n" "$badge_info_files_list"

[[ -z $verbose ]] || echo "reading local badge info files..."
updated_badges_count=0
IFS=$(printf ' \n\t')
# shellcheck disable=SC2068
for badge_info_file in $badge_info_files_list; do 
    if [[ -f "$badge_info_file" ]]; then
        [[ -z $verbose ]] || echo "processing: $(realpath "$badge_info_file")"

        badge_id=""
        badge_value=""
        badge_name_json=${badge_info_file##*/badge.}
        badge_name=${badge_name_json%.json}
        badge_label=$(jq -r '.label' <"$badge_info_file")

        if [[ "$badge_label" = "null" || "$badge_label" = '' ]]; then
            echo "invalid badge info file! skipping: ${badge_info_file}"
            continue
        fi

        badge_id=$(jq  '.[] | select(.name == "'"$badge_name"'") .id' < "$TMP_FILE" || true)

        [[ -z $verbose ]] || echo "badge name: $badge_name"
        [[ -z $verbose ]] || echo "badge label: $badge_label"

        badge_value=$(jq -r '.message' <"$badge_info_file")
        [[ -z $verbose ]] || echo "badge value: $badge_value"

        badge_link=$CI_JOB_URL
        [[ -z $verbose ]] || echo "badge link: $badge_link"

        badge_color=$(jq -r '.color' <"$badge_info_file")
        [[ -z $verbose ]] || echo "badge color: $badge_color"

        badge_img_url="https://${SHIELDS_SERVER}/badge/$badge_label-$badge_value-$badge_color"

        if [[ -n "$badge_value" ]]; then
            if [[ -z "$badge_id" ]]; then
                [[ -z $verbose ]] || echo "no badge named $badge_name was found! adding a new badge..."
                curl -s -S --fail --request POST --header "PRIVATE-TOKEN: ${CI_API_TOKEN}" --data "link_url=${badge_link}&image_url=${badge_img_url}&name=${badge_name}" \
                    "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/badges"
            else
                [[ -z $verbose ]] || echo "badge id: $badge_id"
                [[ -z $verbose ]] || echo "updating badge..."
                curl -s -S --fail --request PUT --header "PRIVATE-TOKEN: ${CI_API_TOKEN}" --data "image_url=${badge_img_url}&link_url=${badge_link}" \
                    "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/badges/${badge_id}" | jq || curl -v --request PUT --header "PRIVATE-TOKEN: ${CI_API_TOKEN}" --data "image_url=${badge_img_url}&link_url=${badge_link}" \
                    "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/badges/${badge_id}" 
            fi
            printf "\n"
            
            # verify badge link is updated on the server
            curl -s -S --fail --header "PRIVATE-TOKEN: ${CI_API_TOKEN}" "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/badges" > "$TMP_FILE"
            updated_badge_link=$(jq  '.[] | select(.name == "'"$badge_name"'") .link_url' < "$TMP_FILE" || true)
            if [[ "$updated_badge_link" == "\"$badge_link\"" ]]; then
                [[ -z $verbose ]] || echo "badge link verified!"
                updated_badges_count=$(( updated_badges_count + 1 ))
            else
                echo "ERROR: GitLab server's response did not get updated after our request!"
                echo "> expected link_url for $badge_name: $badge_link"
                echo "> updated link_url for $badge_name: $updated_badge_link"
                echo "> GitLab API response:"
                cat "$TMP_FILE"
                exit 1
            fi
        fi
    fi 
done

[[ -z $verbose ]] || echo "updated badges: $updated_badges_count"

if [[ "$updated_badges_count" == "0" ]]; then
    echo "ERROR: no valid badge info file was found! no badge got updated."
    exit 1
fi
