#!/bin/bash
set -e

badger.sh --verbose --badge-info="tests/badge.dead_internal_links.json" --badge-file="badges/dead_internal_links.svg"
badger.sh --verbose --badge-info="tests/badge.dead_external_links.json" --badge-file="badges/path/to/dead_external_links.svg"
badger.sh --verbose --badge-info="tests/badge.http_links.json" --badge-file="badges/http_links.svg"
grep -q "<svg " badges/dead_internal_links.svg
grep -q "<svg " badges/path/to/dead_external_links.svg