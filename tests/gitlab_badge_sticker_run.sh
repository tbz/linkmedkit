#!/bin/bash
set -e

echo "** Testing: verbose mode"
gitlab_badge_sticker.sh --verbose

echo "** Testing: default execution"
gitlab_badge_sticker.sh

echo "** Testing: missing badge file"
cd "$(mktemp -d)"
if gitlab_badge_sticker.sh; then false; fi

echo "** Testing: invalid badge file"
touch badge.test.json
if gitlab_badge_sticker.sh; then false; fi

echo "** Testing: missing API token"
unset CI_API_TOKEN
if gitlab_badge_sticker.sh; then false; fi

echo "** Testing: missing env"
if env -i "$(which gitlab_badge_sticker.sh)"; then false; fi
